<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<script src='https://code.jquery.com/jquery-3.5.1.min.js'></script>
<style>
	body{
		margin: 30px;
	}
	.calendar{
		position: relative;
		height: 60%;
		width: 90%;
		align-items: center;
	}
	.calendar>TBODY>TR>TD{
		min-height: 250px;
		cursor: pointer;
	}
	.dayInCal{
		font-size: 12px;
	}
	.controller{
		width: 100%;
		text-align: center;
		align-content: center;
	}
	TR:nth-child(even){background-color: #ffe; }
	TD,TH{
		border: 1px #aaa solid;
	}
</style>
<script>
	var dates = Array();
	function setDate(id){
		if(dates.indexOf(id) !== -1){
			//console.log("Már van");
			var index = dates.indexOf(id);
			$("#"+id+"").css("background-color","#fff");
			dates.splice(index, 1);
		}
		else{
			//console.log("Még nincs" +id);
			$("#"+id+"").css("background-color","#afa");
			dates.push(id);	
		}
		//console.log(dates);
		datesToDisplay = "";
		for(i=0;i<dates.length;i++){
			datesToDisplay = datesToDisplay + "<li>" + dates[i].substring(0,4)+"."+dates[i].substring(4,6)+"."+dates[i].substring(6,8) + "</li>";
		}
		//$("#napok").html("<b>Kiválasztott napok: </b>"+dates.toString(" "));
		$("#napok").html("<b>Kiválasztott napok: </b>"+datesToDisplay);
		if(dates.length==0) $("#napok").html("");
	}
</script>
<?php
// 4 heti mentés van.
	if(isset($_GET["date"]) && $_GET["date"]!=""){
		$dat = explode("-",$_GET["date"]);
		$actYear=$dat[0];
		$actMonth=$dat[1];
		$date = $actYear."-".$actMonth;
	}
	else{
		$actYear = date("Y");
		$actMonth = date("m");	
		$date = $actYear."-".$actMonth;
	}
	?>
	<div>
		<div id='napok' style='float:left'></div>
		<div id='calendar_container'></div>
	<div>
	<script type="text/javascript">
		function loadData(datum) {
		//alert( datum);
		console.log(dates);
		$( "#calendar_container" ).html("");
		$.get("calendar_backend.php?date="+datum, function( data ) {
		  $( "#calendar_container" ).html( data );
		  for(i=0;i<dates.length;i++){
		  		$("#"+dates[i]+"").css("background-color","#afa");
		  }
		});
	}
	</script>
	<?php
	echo "<script>";
		echo "loadData(\"".$date."\");";
	echo "</script>";
	?>
	<button id='tovabb'>Tovább</button>

	<div id='result'></div>
	<script>
	$( "#tovabb" ).click(function() {
		alert(dates);
		$.post( "calendar_backend.php?act=post", { dates: dates })
		  .done(function( data ) {
		    alert( "Sikeres mentés a következő dátumokra: " + data );
		  });
	});
	</script>
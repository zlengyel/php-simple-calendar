<?php
	if(isset($_GET["date"]) && $_GET["date"]!=""){
		$dat = explode("-",$_GET["date"]);
		$actYear=$dat[0];
		$actMonth=$dat[1];
	}
	else{
		$actYear = date("Y");
		$actMonth = date("m");	
	}

	if(isset($_GET["act"]) && $_GET["act"]!=""){
		$act = $_GET["act"];
	}
	else{
		$act="";
	}

	if($act==""){
	//$actMonth = 4;
	//nem használunk 0 indexet
	$_napokH = Array("","Hétfő","Kedd","Szerda","Csütörtök","Péntek","Szombat","Vasránap");
	$_napokR = Array("","H","K","Sze","Cs","P","Szo","V");

	$return = "";
	$daysInActMonth = cal_days_in_month(CAL_GREGORIAN, $actMonth, $actYear); // 31
	
	
	/*
	
	
	$return.= "<hr>";

	$daysInActMonth = cal_days_in_month(CAL_GREGORIAN, $actMonth, $actYear); // 31
	$return.= "There is {$daysInActMonth} days in {$actYear} {$actMonth} ";
	$return.= "<hr>";	
	*/
	// 	0 (for Sunday) through 6 (for Saturday)
	$lastDayOfMonth = date("t",strtotime($actYear."-".$actMonth));
	$positionFirstDayOfMonth = date("w",strtotime($actYear."-".$actMonth."-01"));
	$positionLastDayOfMonth = date("w",strtotime($actYear."-".$actMonth."-".$lastDayOfMonth));
	
	//cella száma szerinti átmozgatás 0 -> 7
	if($positionFirstDayOfMonth==0) $positionFirstDayOfMonth = 7;
	if($positionLastDayOfMonth==0) $positionLastDayOfMonth=7;
	/*
	$return.= "Start @ ".$positionFirstDayOfMonth.", ami ".$_napokH[$positionFirstDayOfMonth].", tehát ".$_napokR[$positionFirstDayOfMonth];
	$return.= "<br>";
	$return.= "End @ ".$positionLastDayOfMonth.", ami ".$_napokH[$positionLastDayOfMonth].", tehát ".$_napokR[$positionLastDayOfMonth];
	
	$return.= "<hr>";	
		$return.= $elozoHonapNapjai = $positionFirstDayOfMonth-1;
		$return.= "<br>";
		$return.= $kovetkezoHonapNapjai = 7-$positionLastDayOfMonth;
	$return.= "<hr>";	
	*/
	$elozoHonapNapjai = $positionFirstDayOfMonth-1;
	$kovetkezoHonapNapjai = 7-$positionLastDayOfMonth;
	$actMonthDate = date("Y-m",strtotime("{$actYear}-{$actMonth}-01"));
	$nextMonthDate = date("Y-m",strtotime("{$actYear}-{$actMonth}-01 +1 month"));
	$prevMonthDate = date("Y-m",strtotime("{$actYear}-{$actMonth}-01 -1 month"));
	
	/*
	$return.= "<a href='?date={$prevMonthDate}'>&laquo; előző hónap</a> | ";
	$return.= "{$actMonthDate} | ";
	$return.= "<a href='?date={$nextMonthDate}'>következő hónap &raquo; </a> | ";
	*/
	$return.= "<div class='controller'><a href='javascript:loadData(\"{$prevMonthDate}\")'>&laquo; előző hónap</a> | ";
	$return.= "{$actMonthDate} | ";
	$return.= "<a href='javascript:loadData(\"{$nextMonthDate}\")'>következő hónap &raquo; </a> </div>";

	//$return.= $actYear."-".$actMonth;

	/*
	$return.= "<hr>";	
	$return.= "<div id='napok'>Kiválasztott napok</div>";
	$return.= "<hr>";		
	*/
	$naptariNap = 1;
	$return.= "<table class='table calendar' border='1'>";
		$return.= "<thead>";
			$return.= "<tr>";
				foreach($_napokH as $nap){
					if($nap!="") $return.= "<th scope='col'>{$nap}</th>";
				}
			$return.= "</tr>";
		$return.= "</thead>";
		$return.= "<tbody>";
			//first row
			$return.= "<tr>";
			for($i=1;$i<=$elozoHonapNapjai;$i++){
				$return.= "<th></th>";
			}
			for($naptariNap=1;$naptariNap<=(7-$elozoHonapNapjai);$naptariNap++){
				$naptariNapKiir = "0".$naptariNap;
				$return.= "<td id='".$actYear.$actMonth.$naptariNapKiir."' onclick='setDate(this.id)'><span class='dayInCal'>{$naptariNapKiir}</span></td>";
			}
			$return.= "</tr>";

			$sorSzam = 1;
			while($naptariNap<=($daysInActMonth+$kovetkezoHonapNapjai)){
				if($sorSzam==1) $return.= "<tr>";
				if($naptariNap<=$daysInActMonth){
					if($naptariNap<10) $naptariNapKiir = "0".$naptariNap;
					else $naptariNapKiir = $naptariNap;
					$return.= "<td id='".$actYear.$actMonth.$naptariNapKiir."' onclick='setDate(this.id)'><span class='dayInCal'>".$naptariNapKiir."</span></td>";
					$naptariNap++;
				}
				else {
					$return.= "<td></td>";	
					$naptariNap++;
				}
				if($sorSzam%7==0) $return.= "</tr><tr>";
				//$naptariNap++;
				$sorSzam++;
			}
			$return.= "</tr>";

		$return.= "</tbody>";
	$return.= "</table>";
	echo $return;
}
	if($act=="post"){
		echo implode(",",$_POST["dates"]);
	}
?>